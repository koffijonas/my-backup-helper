# myBackupHelper
Usage: myBackup.bat [-e] <backup> [-bfolder] <folder to backup> [args...]

     -bfolder             Dossier à sauvegarder (Ex: C:\wamp64\...\wordpress).
     -bdb                 Nom de la base de données MySQL à sauvegarder.
     -psave <local|sftp>  Protocole de stockage (local|ftp|googledrive).
     -savefolder          Dossier de sauvegarde du backup.
     -h                   Aide.

# Fichier de configuration

src/config/global.xml

# Exemple :

myBackup.bat -e backup -bfolder C:\monDossier -bdb album -save sftp -savefolder phpSFTP/

ou

php run.php -e backup -bfolder C:\monDossier -bdb album -save sftp -savefolder phpSFTP/