<?php
namespace Kjk\classes;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */


/**
 * Description of SFTPConnection
 *
 * @author kjkoffi
 */
class SFTPConnection {
    private $connection;
    private $sftp;
    private $host;

    public function __construct($host, $port=22){
        $this->connection = \ssh2_connect($host, $port);
        if (!$this->connection) {
            throw new \Exception("Impossible de se connecter à $host sur $port.");
        }
        $this->host = $host;
    }

    public function login($username, $password){
        if (!\ssh2_auth_password($this->connection, $username, $password)) {
            throw new \Exception("Impossible de s'authentifier avec le nom d'utilisateur et le mot de passe.");
        }

        $this->sftp = \ssh2_sftp($this->connection);
        if (!$this->sftp) {
            throw new \Exception("Impossible d'initialiser le sous-système SFTP.");
        }
    }

    public function uploadFile($local_file, $remote_file){
        $remote_file = $remote_file.pathinfo($local_file)['basename'];
        //var_dump(pathinfo($local_file['basename'])); die;
        try{
            $sftp = (int)$this->sftp;
	    //var_dump("ssh2.sftp://{$sftp}{$this->resolvePath($remote_file)}");
            $stream = \fopen("ssh2.sftp://{$sftp}{$this->resolvePath($remote_file)}", 'w');

            if (!$stream) {
                throw new \Exception("Impossible d'ouvrir le fichier: $remote_file");
            }

            $data_to_send = \file_get_contents($local_file);
            if ($data_to_send === false) {
                throw new \Exception("Impossible d'ouvrir le fichier local: $local_file.");
            }

            if (\fwrite($stream, $data_to_send) === false) {
                throw new \Exception("Impossible d'envoyer les données du fichier: $local_file.");
            }

            \fclose($stream);
            echo("Fichier transféré vers {$this->host} via SFTP.\n");
            return true;
        }catch (\Exception $e){
            echo $e->getMessage() . "\n";
            return false;
        }
    }
    
    protected function resolvePath($remoteFile){
        if (substr($remoteFile, 0, 1) != '/') {
            //resolve relative path
            $absolutePath = \ssh2_sftp_realpath($this->sftp, ".");
            $remoteFile = $absolutePath . '/' . $remoteFile;
        }
        return $remoteFile;
    }
}
