<?php
namespace Kjk\classes;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

use Ifsnop\Mysqldump as IMysqldump;
/**
 * Description of MySQLDump
 *
 * @author kjkoffi
 */
class MySQLDump {
    private $config;

    public function __construct() {
        $this->config = \Kjk\Bootstrap::$config->db->mysql;
        
//        $dsn = $config['db']['dsn'];
//        $user = $config['db']['username'];
//        $password = $config['db']['password'];
        //var_dump($dsn); die;

        
    }
    
    public function export($dbName, $dumpName='Sauvegarde', $dumpLocation='') {
        $dbName = empty($dbName) ? $this->config->dbname : $dbName;
        $dump = new IMysqldump\Mysqldump(
            "mysql:dbname={$dbName};host={$this->config->host};charset=utf8", 
            $this->config->username, 
            $this->config->password
        );
        
        //------------------------------------
        echo("Début de l'export SQL...\n");
        $dump->start($dumpLocation.$dumpName.'_'.date('Ymd').'.sql');
        echo("Export SQL terminé.\n");
        
        return $dumpLocation.$dumpName.'_'.date('Ymd').'.sql';
    }
}
