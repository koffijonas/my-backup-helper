<?php
namespace Kjk\classes;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

use Alchemy\Zippy\Zippy;

/**
 * Description of Compresser
 *
 * @author kjkoffi
 */
class Compresser {
    /**
     * @var Zippy
     */
    private $zipper;
    /**
     * @var \Alchemy\Zippy\Archive\ArchiveInterface
     */
    public $fileZip;


    public function __construct() {
        $this->zipper = Zippy::load();
    }
    
    public function zip($zipName, $folders, $pathToSave='', $args=null) {
        try {
			//Création
            $this->fileZip = $this->zipper->create("{$pathToSave}{$zipName}.zip", ['README.md'=>'README.md']);
			foreach($folders as $name => $item){
				$this->fileZip->addMembers([$name => $item]);
			}
			$this->fileZip->removeMembers('README.md');
            echo "Zip créé : {$pathToSave}{$zipName}.zip".PHP_EOL;
			
            return "{$pathToSave}{$zipName}.zip";
            
        } catch (\Exception $exc) {
            //echo $exc->getTraceAsString();
            echo $exc->getMessage().PHP_EOL;
            //echo PHP_EOL."{$pathToSave}{$zipName}.zip".PHP_EOL;
            return false;
        }
        
    }
    
    public function unzip($param) {
        
    }
	
    public function close() {
        $this->fileZip = null;
    }
}
