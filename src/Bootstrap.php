<?php
namespace Kjk;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Kjk\classes\Compresser;
/**
 * Description of Bootstrap
 *
 * @author kjkoffi
 */
class Bootstrap {
    private $params;
    public static $config;


    public function __construct($params) {
        $this->params = $params;
        self::$config = simplexml_load_file(__DIR__ . '/config/global.xml');
    }
    
    public function run() {
        
        //Format params
        $args = [];
        foreach ($this->params as $index => $value) {
            if(preg_match('#^-#', $value)){
                $args[$value] = @$this->params[$index+1];
            }
        }
        
        //Récupération éventuelle des commandes du fichier de configuration "global.xml"
        if(!isset($args['-bfolder']) && !empty((String)self::$config->cmdargs->bfolder)){
            $args['-bfolder'] = (String)self::$config->cmdargs->bfolder;
        }
        if(!isset($args['-bdb']) && !empty((String)self::$config->cmdargs->bdb)){
            $args['-bdb'] = (String)self::$config->cmdargs->bdb;
        }
        if(!isset($args['-psave']) && !empty((String)self::$config->cmdargs->psave)){
            $args['-psave'] = (String)self::$config->cmdargs->psave;
        }
        if(!isset($args['-savefolder']) && !empty((String)self::$config->cmdargs->savefolder)){
            $args['-savefolder'] = (String)self::$config->cmdargs->savefolder;
        }
        
        //Traitement de la commande
        if(key_exists('-e', $args) && (key_exists('-bfolder', $args) || key_exists('-bdb', $args)) && key_exists('-psave', $args) && key_exists('-savefolder', $args)){
            switch ($args['-e']) {
                case 'zip':
                    $archive = new Compresser();
                    $archive->zip($args['-n'], $args['-s'], $args['-o']);
                    break;
                
                case 'mysql':
                    $mysql = new classes\MySQLDump();
                    $dumpFile = $mysql->export($args['-db'], $args['-f'], $args['-o']);
                    break;
                
                case 'sftp':
                    $sftp = new classes\SFTPConnection(self::$config->sftp->host, self::$config->sftp->port);
                    $sftp->login(self::$config->sftp->username, self::$config->sftp->password);
                    $sftp->uploadFile($args['-f'], $args['-o']);
                    break;
                
                case 'backup':
                    $zipAttach = [];
                    
                    // bdb = backup data base
                    if(isset($args['-bdb'])){
                        //Export de la base de données
                        $mysql = new classes\MySQLDump();
                        $sqlFile = $mysql->export($args['-bdb'], "Sauvegarde-MySQL-".$args['-bdb'], 'Sql'.DIRECTORY_SEPARATOR);
                        $zipAttach[pathinfo($sqlFile)['basename']] = $sqlFile;
                    }
                    
                    // bfolder = backup folder
                    if(isset($args['-bfolder'])){
                        $pathFolderCode = $args['-bfolder'];
                        $zipAttach[pathinfo($pathFolderCode)['basename']] = $pathFolderCode;
                    }

                    if(count($zipAttach)>0){
                        //Création du Zip
                        $archive = new Compresser();
                        $bkName = "Backup-".date('Ymdhis');
                        $zipFile = $archive->zip($bkName, $zipAttach, 'Zip'.DIRECTORY_SEPARATOR);
                        $archive->close(); //Close zip file

                        if($args['-psave']==='sftp'){
                                //Envoi du zip par sftp
                                $sftp = new classes\SFTPConnection(self::$config->sftp->host, (int)self::$config->sftp->port);
                                $sftp->login(self::$config->sftp->username, self::$config->sftp->password);
                                $args['-savefolder'] = in_array($args['-savefolder'], ['.','./','/']) ? '' : $args['-savefolder'];
                                $sftp->uploadFile($zipFile, $args['-savefolder']);

                        }else if($args['-psave']==='local'){
                                copy($zipFile, $args['-savefolder'].DIRECTORY_SEPARATOR.pathinfo($zipFile)['basename']);
                        }
                    }
                    
                    //Supprission des fichiers temporaires
                    if (!empty($sqlFile)) unlink($sqlFile);
                    if (!empty($zipFile)) unlink($zipFile);
                    
                    break;
                //default:
                    
            }
        }else{
            echo "Usage: myBackup [-e] <backup> [-bfolder] <folder to backup> [args...]".PHP_EOL
                . "     -bfolder             Dossier à sauvegarder (Ex: C:\wamp64\...\wordpress).".PHP_EOL
                . "     -bdb                 Nom de la base de données MySQL à sauvegarder.".PHP_EOL
                . "     -psave <local|sftp>  Protocole de stockage (local|sftp|googledrive).".PHP_EOL
                . "     -savefolder          Dossier de sauvegarde du backup.".PHP_EOL
                . "     -h                   Aide.".PHP_EOL.PHP_EOL;
        }
        
    }
}
