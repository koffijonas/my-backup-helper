<?php
namespace Kjk\config\db;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SQLite
 *
 * @author kjkoffi
 */
class SQLite {
    /**
     * @var \PDO 
     */
    public $pdo;
            
    function __construct() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO('sqlite:'.__DIR__.'/exportIntrementalReg.db');
            //$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
        }
        
        /*if ($this->cnx == null) {
            $this->cnx = new \SQLite3(__DIR__.'/timeupLeShow.db');
        }*/
        $this->setResultObject();
    }
    
    public function setResultArray() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }
    
    public function setResultObject() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    }
    
    public function fetchAll($sql, $isAll=false) {
        $select = $this->pdo->query($sql);
        return $isAll ? $select->fetchAll() : $select->fetchObject(); // fetchObject(SQLITE3_ASSOC);
    }
    
    public function insert($sql, $values=null) {
        $stmt = $this->pdo->prepare($sql/*"INSERT INTO posts (titre, created) VALUES (:titre, :created)"*/);
        if(empty($values)){
            $result = $stmt->execute();
        }else{
            $result = $stmt->execute($values);
        }
        
        return $result;
    }
    
    public function update($sql, $values=null) {
        return $this->insert($sql, $values);
        //return $this->pdo->query($sql);
    }
    
    public function close() {
        $this->pdo = null;
    }
}
