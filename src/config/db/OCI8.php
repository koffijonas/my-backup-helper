<?php
namespace Kjk\config\db;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MySQL
 *
 * @author kjkoffi
 */
class OCI8 {
    /**
     * @var \PDO 
     */
    public $cnx;
    
    public $db_hostname="172.16.0.172";
    public $db_sid="sigicidev";
    public $db_username="di";
    public $db_password="di123d";
    const obj_user = 'sigici';


    
    function __construct($sid='sigicidev') {
        
        if($sid === 'pprd12c'){
            $this->db_hostname = '172.16.0.201';
            $this->db_username = 'sigici';
            $this->db_password = 'sigici';
            $this->db_sid = 'pprd12c';
            
        }else if($sid === 'orakjk'){
            $this->db_hostname = 'oracle.kjk.ci';
            $this->db_username = 'sigici';
            $this->db_password = 'sigici';
            $this->db_sid = 'sigtest';
        }
        
        $tns = [
            'sigicidev' => "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = {$this->db_hostname})(PORT = 1521))) (CONNECT_DATA =(SID = {$this->db_sid})))",
            'pprd12c' =>"(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = {$this->db_hostname})(PORT = 1521))) (CONNECT_DATA =(SID = {$this->db_sid})))",
            'orakjk' =>"(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = {$this->db_hostname})(PORT = 1521))) (CONNECT_DATA =(SID = {$this->db_sid})))"
        ];
        
        $this->cnx = oci_connect($this->db_username, $this->db_password, $tns[$sid], 'AL32UTF8') or die("Connexion à {$this->db_hostname} impossible.");
    }
    
    public function setResultArray() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }
    
    public function setResultObject() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    }
    
    public function query($sql) {
        $stmt = oci_parse($this->cnx, $sql);
        oci_execute($stmt, OCI_DEFAULT);
        return $stmt;
    }
    
    
    function __destruct() {
        \oci_close($this->cnx);
    }
}
