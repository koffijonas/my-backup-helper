<?php
namespace kjk\config\db;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MySQL
 *
 * @author kjkoffi
 */
class MySQL {
    /**
     * @var \PDO 
     */
    public $pdo;
    
    public $db_hostname="localhost";
    public $db_name="immobilier-plus";
    public $db_username="root";
    public $db_password="root";
            
            
    function __construct() {
        if ($this->pdo == null) {
            $this->pdo = new \PDO("mysql:host={$this->db_hostname};dbname={$this->db_name};charset=UTF8", $this->db_username, $this->db_password);
            //$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
        }
        
        /*if ($this->cnx == null) {
            $this->cnx = new \SQLite3(__DIR__.'/timeupLeShow.db');
        }*/
    }
    
    public function setResultArray() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }
    
    public function setResultObject() {
        $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
    }
    
    public function query($sql) {
        $select = $this->pdo->query($sql);
        return $select;
    }
}
