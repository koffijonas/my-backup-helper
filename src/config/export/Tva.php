<?php
namespace Kjk\config\export;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//use Kjk\config\export\Commun;
/**
 * Description of Liasse
 *
 * @author kjkoffi
 */
class Tva extends Commun {
    const tableName = 'v_tva_for_ins';

    public function __construct($args) {
        parent::__construct($args);
        
        //Gestion de l'export incremental
        if(isset($args['-itag'])){
            $this->tagIncrDateExport('tva', $this->args['-f']);
            $this->wlog(PHP_EOL."Incremental dateExport Tag OK.".PHP_EOL);
            return;
        }
        $incrementalWhere = '';
        if(isset($args['-i']) && $args['-i']==='yes'){
            $lastDateExport = $this->getLastDateTrans('tva', $this->args['-f']);
            $incrementalWhere = !empty($lastDateExport) ? " AND TO_CHAR(DATE_TRANSMISSION, 'YYYY-MM-DD HH24:MI:SS') > '{$lastDateExport}' " : $incrementalWhere;
        }
        
        $this->tableName = self::tableName;
        $this->wlog("Execution de la vue '".self::tableName."'...");
	#die("SELECT * FROM {$this->ociObjUser}.".self::tableName." t WHERE 1=1 {$incrementalWhere} ");
        $data = $this->db->query("SELECT * FROM {$this->ociObjUser}.".self::tableName." t WHERE 1=1 {$incrementalWhere} ");
        $this->wlog("OK.".PHP_EOL.'0% ');
        $this->fileSurfix = "_{$this->exportRef}";
        $this->setFormatExport($args, $data);
        //$this->saveOnSqLiteTva($listLiass);
        oci_free_statement($data);
        
        
        //End loading date('h:i:s',abs($date1 - date()))
        $this->wlog('=> 100%'.PHP_EOL);
        $interval = $this->beginDate->diff(new \DateTime());
        $this->wlog("Temps d'execution : ".($interval->format('%h:%i:%s')).PHP_EOL);
    }
}
