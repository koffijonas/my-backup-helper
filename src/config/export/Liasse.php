<?php
namespace Kjk\config\export;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//use Kjk\config\db\MySQL;
//use Kjk\config\db\SQLite;
use Kjk\config\db\OCI8;
/**
 * Description of Liasse
 *
 * @author kjkoffi
 */
class Liasse extends Commun {
    const tableName = 'v_liasse_for_ins';
    private $liasseType;

    public function __construct($args) {
        parent::__construct($args);
        
        //Récupération de du nom de la vue / table
        $this->tableName = self::tableName;
        
        //Gestion de l'export incremental
        if(isset($args['-itag'])){
            $this->tagIncrDateExport('liasse', $this->args['-f']);
            $this->wlog(PHP_EOL."Incremental dateExport Tag OK.".PHP_EOL);
            return;
        }
        $incrementalWhere = '';
        if(isset($args['-i']) && $args['-i']==='yes'){
            $lastDateExport = $this->getLastDateTrans('liasse', $this->args['-f']);
            $incrementalWhere = !empty($lastDateExport) ? " AND TO_CHAR(DATE_VALIDATION, 'YYYY-MM-DD HH24:MI:SS') > '{$lastDateExport}' " : $incrementalWhere;
            //var_dump($lastDateExport); die;
        }
        
        $nccWhere = '';
        if( isset($args['-ncc']) && !empty($args['-ncc']) ){
            $nccWhere .= " AND NCC='".$args['-ncc']."'";
        }
        
        //Gestion des exports par partition
        $this->wlog("Execution de la vue '".self::tableName."'...");
        //Récupération de la liste des NCC concerné par l'export
        $dataNcc = $this->db->query("SELECT imp.ncc AS LISTE_NCC, LISTAGG(imp.impot_id || ',' || imp.exercice,';')WITHIN GROUP(ORDER BY imp.exercice) AS DETAIL "
                                  . "FROM {$this->ociObjUser}.t_impot imp "
                                  . "WHERE imp.impot_type_id LIKE 'LIAS%' AND impot_statut_id = 2 {$incrementalWhere} {$nccWhere} GROUP BY imp.ncc");
        oci_fetch_all($dataNcc, $dataArray);
        //Affichage du début d'une bar de progression
        $this->wlog("OK.".PHP_EOL."0% ");
        
        //Declaration de variables
        $listeNcc = ''; $cptNcc = 0; $this->filePart = 0; $cptAllNcc = 0; $isSkipOld = false;
        //$listLiass = [];
        $this->nombreNcc = count($dataArray['LISTE_NCC']);//Nombre d'NCC récupéré
        //var_dump($this->nombreNcc); die;
        
        
        $args['-sz'] = isset($args['-sz']) ? $args['-sz'] : 4000;
        
        //if(isset($args['-sz'])){//Traitement avec limitation de la taille du fichier
            //Surfix du fichier à enregistrer
            $this->fileSurfix = "_{$this->exportRef}_({$this->nombreNcc})_part{$this->filePart}";
            foreach ($dataArray['LISTE_NCC'] as $index => $ncc) {
                $cptAllNcc++;
                $cptNcc++;
                $listeNcc .= empty($listeNcc) ? "'{$ncc}'" : ",'$ncc'";
                //$listLiass[] = $this->getDetailLiasse($ncc, $dataArray['DETAIL'][$index], $listLiass);

                if($cptNcc===1000 || $index===($this->nombreNcc-1)){//L'export ce fait par maxi de 1000 NCC
                    //Définition des colonnes à sélectionner
                    $colonne = '*';
                    if(isset($args['-cg']) && $args['-cg']=='1'){
                        $this->fileSurfix = "_{$this->exportRef}_infos_generales_({$this->nombreNcc})_part{$this->filePart}";
                        $this->colGroupTag = "infos_generales";
                        $colonne = 'DISTINCT CODE_DEPOT,NCC,RAISON_SOCIALE,SIGLE,ADRESSE,NUMERO_TELEDECLARANT,TELEPHONE,LIBELLE_ACTIVITE,POSTE_COMPTABLE_ID,POSTE_COMPTABLE_LIBELLE,TYPE_LIASSE,EXERCICE,EXPERTS_COMPTABLES';
                    }else if(isset($args['-cg']) && $args['-cg']=='2'){
                        $this->fileSurfix = "_{$this->exportRef}_tableaux_({$this->nombreNcc})_part{$this->filePart}";
                        $this->colGroupTag = "tableaux";
                        $colonne = 'CODE_DEPOT,NCC,ORDRE_TABLEAU,NOM_TABLEAU,CODE,LIBELLE_ID,LIBELLE,VALEUR';
                    }
                    
                    //Définition de la close WHERE
                    $where = isset($args['-y']) ? " AND EXERCICE='".$args['-y']."'" : '';
                    $where .= isset($args['-tl']) ? " AND TYPE_LIASSE='".$this->getLiasseType($args['-tl'])."'" : '';
                    $where .= str_replace('DATE_VALIDATION', "TO_DATE(DATE_TRANSMISSION, 'YYYY-MM-DD HH24:MI:SS')", $incrementalWhere);
                    
                    //Execution de la requête pour exportation 
                    $data = $this->db->query("SELECT {$colonne} FROM {$this->ociObjUser}.".self::tableName." t WHERE t.ncc in ({$listeNcc}) {$where} ORDER BY t.ncc");
                    //Appel de la fonction d'export
                    $this->setFormatExport($args, $data, (isset($args['-sz'])?$args['-sz']:null) );
                    //Liberation de la ressource oracle
                    oci_free_statement($data);
                    //Affichage de la progression de l'export en %
                    $this->wlog('('.((int)(($index*100)/$this->nombreNcc)).'%)');//
                    
                    $cptNcc=0;
                    $listeNcc = '';
                }
            }
            
        //}
        
        //End loading
        $this->wlog('=> 100%');
        $interval = $this->beginDate->diff(new \DateTime());
        $this->wlog(PHP_EOL."Temps d'execution : ".($interval->format('%h:%i:%s')).PHP_EOL);
    }
    
    public function getLiasseType($code) {
        /*$liasseType = [
            "NO" => "Etats financiers système normal",
            "AV" => "Etats financiers système assurance vie",
            "AN" => "Etats financiers système assurance non-vie",
            "BA" => "Etats financiers système bancaire",
            "MF" => "Etats financiers systèmes financiers décentralisés de la zone UMOA",
            "MT" => "Etats financiers système minimal de trésorerie"
        ];*/
        $liasseType = null;
        if(empty($this->liasseType)){
            $smt = $this->db->query("SELECT IMPOT_TYPE_ID, LIBELLE FROM {$this->ociObjUser}.t_impot_type WHERE IMPOT_TYPE_ID LIKE 'LIAS%'");
            //oci_fetch_all($data, $liasseType);
            while ($item = oci_fetch_object($smt)) {
                $liasseType[$item->IMPOT_TYPE_ID] = $item->LIBELLE;
            }
            $this->liasseType = $liasseType;
            
        }else{
            $liasseType = $this->liasseType;
        }
        
        return @$liasseType[strtoupper("LIAS{$code}")];
    }
}
