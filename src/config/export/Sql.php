<?php
namespace Kjk\config\export;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//use Kjk\config\db\MySQL;
//use Kjk\config\db\SQLite;
use Kjk\config\db\OCI8;
/**
 * Description of Liasse
 *
 * @author kjkoffi
 */
class Sql extends Commun {

    public function __construct($args, $sql) {
        parent::__construct($args);
        
        $incrementalWhere = '';
        $liasseDejaExporte = [];
        if(isset($args['-i']) && $args['-i']==='yes'){
            $liasseDejaExporte = $this->getAllLiasseAbandon($args['-f']);
            $liasseDejaExporte = $liasseDejaExporte==null ? [] : $liasseDejaExporte;
            //var_dump(in_array('28928505', $liasseDejaExporte)); die;
            //$incrementalWhere = !empty($last_impot_id) ? " WHERE DECLARATION_ID > {$last_impot_id} " : $incrementalWhere;
        }
        
        $this->tableName = $args['-e'];
        $this->wlog("Execution de du fichier '".$args['-e']."'...");
        $data = $this->db->query($sql);
        $this->wlog("OK.".PHP_EOL.'0% ');
        $this->fileSurfix = "_{$this->exportRef}";
        $this->setFormatExport($args, $data, null, $liasseDejaExporte);
        //$this->saveOnSqLiteTva($listLiass);
        oci_free_statement($data);
        
        $this->saveLiasseAbandon(null, $args['-f'], $this->listImpotId);
        
        //End loading date('h:i:s',abs($date1 - date()))
        $this->wlog('=> 100%'.PHP_EOL);
        $this->wlog(' Nombre de dépôt : '.(count($this->listImpotId)).PHP_EOL);
        $interval = $this->beginDate->diff(new \DateTime());
        $this->wlog("Temps d'execution : ".($interval->format('%h:%i:%s')).PHP_EOL);
    }
}
