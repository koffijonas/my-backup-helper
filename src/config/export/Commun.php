<?php
namespace Kjk\config\export;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Kjk\config\db\SQLite;
use Kjk\config\db\OCI8;
/**
 * Description of Commun
 *
 * @author KJKOFFI
 */
class Commun {
    public $db;
    public $args;
    public $filePath;
    public $filePart;
    public $fileName;
    public $fileExt;
    public $fileSurfix;
    public $loading;
    public $beginDate;
    public $exportRef;
    public $tableName;
    public $nombreNcc;
    public $ociObjUser;
    public $colGroupTag;
    public $isWriteHeaderInFile;
    public $lastImpotId;
    public $listImpotId;


    public function __construct($args) {
        $tns = isset($args['-sid']) ? $args['-sid'] : 'sigicidev';
        $this->db = new OCI8($tns);
        $this->ociObjUser = OCI8::obj_user;
        $this->wlog("Connexion à la base de donnée ($tns) OK.".PHP_EOL);
        
        $this->exportRef = isset($args['-er']) ? $args['-er'] : date('Ymd');
        
        $this->filePath = (isset($args['-o'])?$args['-o'].'/':'');
        $this->fileName = (isset($args['-i']) ? 'Incremental-' : '').'Export-'.$args['-e'].'-data'.(isset($args['-y'])?'-'.$args['-y']:'');
        
        $args['-f'] = isset($args['-f']) ? $args['-f'] : 'csv';
        $this->args = $args;
        
        //Begin loading
        if(!file_exists($this->filePath.'log_export.log')){
            file_put_contents($this->filePath.'log_export.log', '');
        }
        $this->wlog('Debut de l\'export : '.date('d/m/Y H:i:s').PHP_EOL);
        $this->loading = 0;
        $this->beginDate = new \DateTime();
        
        $this->lastImpotId = 0;
        $this->listImpotId = [];
    }
    
    public function getDetailLiasse($ncc, $detail, &$listLiass) {
        $detailLiasse = explode(';', $detail);
        foreach ($detailLiasse as $item) {
            $imp = explode(',', $item);
            $listLiass[] = [
                'ncc' => $ncc,
                'impot_id' => $imp[0],
                'exercice' => $imp[1],
            ];
        }
    }
    
    public function getLastImpotId($type, $format='insert', $strict=false) {
        $sqLite = new SQLite();
        //$rs = $sqLite->fetchAll("SELECT max(impot_id) as last_impot_id FROM {$table} WHERE exportFormat='{$format}'");
        $rs = $sqLite->fetchAll("SELECT impot_id as last_impot_id FROM lastImpotId WHERE type='{$type}' AND format='{$format}' AND date_export=date('now')");
        
        if(!empty($rs)){
            return $rs->last_impot_id;
        }else if(!$strict){
            $lastId = ($sqLite->fetchAll("SELECT max(id) as lastId FROM lastImpotId WHERE type='{$type}' AND format='{$format}'"))->lastId;
            $sqLite->update("UPDATE lastImpotId SET date_export=date('now') WHERE id='{$lastId}'");
            //$sqLite->update("INSERT into lastImpotId(type, format, date_maj) VALUES ('{$type}','{$format}',date('now'))");
            $sqLite->update("INSERT into lastImpotId(type, impot_id, format, date_maj) SELECT type, impot_id, format, date('now') FROM lastImpotId WHERE id='{$lastId}'");
            
            return $this->getLastImpotId($type, $format, true);
        }else{
            return null;
        }
    }
    public function tagIncrDateExport($type, $format='insert') {
        $sqLite = new SQLite();
        $rs = $sqLite->fetchAll("SELECT * FROM incrExport WHERE type='{$type}' AND date_export is null", true);
        $this->wlog("SELECT * FROM incrExport WHERE type='{$type}' AND date_export is null".PHP_EOL);
        
        if(!empty($rs)){
            foreach ($rs as $item) {
                //var_dump($item->id); die;
                $sqLite->update("UPDATE incrExport SET date_export=datetime() WHERE id='{$item->id}'");
                $sqLite->update("INSERT into incrExport(type, date_transmission, format, date_export) SELECT type, datetime(), format, null FROM incrExport WHERE id='{$item->id}'");
            }
        }else{
            return null;
        }
    }
    public function getLastDateTrans($type, $format='insert') {
        $sqLite = new SQLite();
        $rs = $sqLite->fetchAll("SELECT strftime('%Y-%m-%d %H:%M:%S', max(date_transmission)) as last_date FROM incrExport WHERE type='{$type}' AND format='{$format}' AND date_export is not null");
        
        if(!empty($rs)){
            return $rs->last_date;
        }else{
            return null;
        }
    }
    
    //public function saveOnSqLite($listLiass, $exportFormat='insert') {
    public function saveOnSqLite($impotId, $exportFormat='insert', $listImpotId=null) {
        $this->wlog("~");
		if($impotId==0){return;} //Empêche l'enregistrement des valeurs null pour eviter des export full
        
        if($this->args['-e']==='liasse'){
            //$this->saveLastImpotIdLiasse($impotId, $exportFormat);
        }else if($this->args['-e']==='tva'){
            //$this->saveLastImpotIdTva($impotId, $exportFormat);
        }else if($this->args['-e']==='liasseAbandon'){
            //$this->saveLiasseAbandon($impotId, $exportFormat, $listImpotId);
        }
    }
    
    public function saveLastImpotIdLiasse($impotId, $exportFormat='insert') {
        $sqLite = new SQLite();
        //$sqLite->update("UPDATE lastImpotId SET liasse=:impotId WHERE 1", ['impotId' => $impotId]);
        $sqLite->update("UPDATE lastImpotId SET impot_id='{$impotId}' WHERE type='liasse' and format='{$exportFormat}' and date_maj=date('now')");
        $sqLite->close();
    }
    
    public function saveLiasseAbandon($listLiass, $exportFormat='insert', $listImpotId) {
        $sqLite = new SQLite();
        foreach ($listImpotId as $impotId) {
            $sqLite->insert("INSERT INTO incrLiasseAbandon(type, impot_id, format, date_export) "
                          . "SELECT :type, :impotId, :format, date() "
                          . "WHERE NOT EXISTS (SELECT 1 FROM incrLiasseAbandon WHERE impot_id=:impotId AND format=:format)" 
            ,[
                'type' => 'liasse',
                'impotId' => $impotId,
                'format' => $exportFormat,
            ]);
        }
        $sqLite->close();
    }
    public function getAllLiasseAbandon($format) {
        $sqLite = new SQLite();
        $rs = $sqLite->fetchAll("SELECT DISTINCT impot_id FROM incrLiasseAbandon WHERE format='{$format}'", true);
        
        if(!empty($rs)){
            $data = [];
            foreach ($rs as $item) {
                $data[] = $item->impot_id;
            }
            return $data;
        }else{
            return null;
        }
    }
    
    public function saveOnSqLiteLiasse($listLiass, $exportFormat='insert') {
        $sqLite = new SQLite();
        foreach ($listLiass as $liasse) {
            $sqLite->insert("INSERT INTO liasse(ncc, exercice, impot_id, file_export, exportFormat) "
                          . "SELECT :ncc, :exercice, :impotId, :fileExport, :exportFormat "
                          . "WHERE NOT EXISTS (SELECT 1 FROM liasse WHERE impot_id=:impotId AND exportFormat='{$exportFormat}')" 
            ,[
                'ncc' => $liasse['ncc'],
                'exercice' => $liasse['exercice'],
                'impotId' => $liasse['impot_id'],
                'fileExport' => $this->getFileName(),
                'exportFormat' => $exportFormat
            ]);
        }
        $sqLite->close();
    }
    
    public function saveLastImpotIdTva($impotId, $exportFormat='insert') {
        $sqLite = new SQLite();
        //$sqLite->update("UPDATE lastImpotId SET tva=:impotId WHERE 1", ['impotId' => $impotId]);
        $sqLite->update("UPDATE lastImpotId SET impot_id='{$impotId}' WHERE type='tva' and format='{$exportFormat}' and date_maj=date('now')");
        $sqLite->close();
    }
    public function saveOnSqLiteTva($listDec, $exportFormat='insert') {
        $sqLite = new SQLite();
        foreach ($listDec as $dec) {
            $sqLite->insert("INSERT INTO tva(ncc, exercice, impot_id, file_export, exportFormat) "
                          . "SELECT :ncc, :exercice, :impotId, :fileExport, :exportFormat "
                          . "WHERE NOT EXISTS (SELECT 1 FROM tva WHERE impot_id=:impotId AND exportFormat='{$exportFormat}')" 
            ,[
                'ncc' => $dec['ncc'],
                'exercice' => $dec['exercice'],
                'impotId' => $dec['impot_id'],
                'fileExport' => $this->getFileName(),
                'exportFormat' => $exportFormat
            ]);
        }
        $sqLite->close();
    }
    
    public function getFileName() {
        if(isset($this->args['-ext'])){
            $this->fileExt = $this->args['-ext'];
        }else if($this->args['-f']==='text'){
            $this->fileExt = '.txt';
        }else if($this->args['-f']==='insert'){
            $this->fileExt = '.sql';
        }else if($this->args['-f']==='excel'){
            $this->fileExt = '.xlsx';
        }
        
        return $this->filePath . $this->fileName . $this->fileSurfix . $this->fileExt;
    }
    
    public function setFormatExport($args, $data, $limitSize=null, $dotExport=[]) {
        switch ($args['-f']) {
            case 'insert':
                $this->exportFormatInsert($data, $limitSize, $dotExport);
                break;
            case 'text':
                $this->exportFormatCsv($data, $limitSize, $dotExport);
                break;
            case 'excel':
                $this->exportFormatExcel($data, $limitSize, $dotExport);
                break;

            default:
                echo "Usage: -f {insert|text|excel}".PHP_EOL;
                break;
        }
    }
    
    public function exportFormatInsert($data, $limitSize=null, $dotExport) {
        //$file = $this->filePath . (empty($this->fileSurfix)?'':'_'.$this->fileSurfix) . '.sql';
        if(empty($limitSize) || !file_exists($this->getFileName())){
            file_put_contents($this->getFileName(), '');//Erase old data
            $fp = fopen($this->getFileName(), 'a');//opens file in append mode
            /////////////fwrite($fp, pack("CCC",0xef,0xbb,0xbf));
        }else{
            $fp = fopen($this->getFileName(), 'a');//opens file in append mode
        }
        
        
        $sql = '';
        $listDec = [];
        //foreach ($data as $item) {
        while ($item = oci_fetch_object($data)) {
            if(isset($item->CODE_DEPOT) && in_array($item->CODE_DEPOT, $dotExport)){ continue; }
            $sqlColomns = '';
            $sqlValues = '';
                    
            foreach ($item as $key => $value) {
                $value = str_replace(PHP_EOL, ' ',str_replace("'", "''", $value));
                $sqlColomns .= empty($sqlColomns) ? $key : ','.$key;
                $sqlValues .= empty($sqlValues) ? "'{$value}'" : ",'{$value}'";
                //Format pour historisation
                if(in_array($key, ['CODE_DEPOT','DECLARATION_ID'/*,'NCC','EXERCICE'*/]) && (int)$value > (int)$this->lastImpotId){
                    //$keyIns = in_array($key, ['CODE_DEPOT','DECLARATION_ID']) ? 'impot_id' : strtolower($key);
                    //$listDec[$item->NCC][$keyIns] = $value;
                    $this->lastImpotId = $value;
                    $this->listImpotId[] = $value;
                }
            }
            
            $sql = "INSERT INTO ".$this->tableName." ({$sqlColomns}) VALUES ($sqlValues); " . PHP_EOL;
            fwrite($fp, $sql);
            //if(!empty($limitSize) && number_format(filesize($this->getFileName())/1048576,2) > ($limitSize-5)){
            if(!empty($limitSize) && number_format($this->getFileSize($fp)/1048576,2) > ($limitSize-2)){
                $this->filePart++;
                $this->fileSurfix = "_{$this->exportRef}_{$this->colGroupTag}_({$this->nombreNcc})_part{$this->filePart}";
                fclose($fp);
                file_put_contents($this->getFileName(), '');//Erase old data
                $fp = fopen($this->getFileName(), 'a');
                /////////////fwrite($fp, pack("CCC",0xef,0xbb,0xbf));
                //break;
            }
            $this->loading();
        }
        //$this->wlog( number_format(filesize($this->getFileName()) / 1048576, 2).'MB' );
        
        fclose($fp);
        //$this->saveOnSqLite($listDec, $this->args['-f']);
        $this->saveOnSqLite($this->lastImpotId, $this->args['-f'], $this->listImpotId);
    }

    public function exportFormatCsv($data, $limitSize=null, $dotExport) {
        //$this->filePath .= (empty($this->fileSurfix)?'':'_'.$this->fileSurfix) . (isset($this->args['-ext']) ? '.'.$this->args['-ext'] : '.csv');
        if(empty($limitSize) || !file_exists($this->getFileName())){
            file_put_contents($this->getFileName(), '');//Erase old data
            $fp = fopen($this->getFileName(), 'a');//opens file in append mode
            /////////////fwrite($fp, pack("CCC",0xef,0xbb,0xbf));
        }else{
            $fp = fopen($this->getFileName(), 'a');//opens file in append mode
        }
        
        $colDelimit = isset($this->args['-dlc']) ? $this->args['-dlc'].'/' : ',';
        $valDelimit = isset($this->args['-dlv']) ? $this->args['-dlv'].'/' : '"';
        
        $isWriteHeader = false;
        $listDec = [];
        //foreach ($data as $item) {
        while ($item = oci_fetch_object($data)) {
            if(isset($item->CODE_DEPOT) && in_array($item->CODE_DEPOT, $dotExport)){ continue; }
            $txtColomns = '';
            $txtValues = '';
                    
            foreach ($item as $key => $value) {
                //$value = utf8_decode(str_replace(PHP_EOL, ' ',str_replace($valDelimit, " ", $value)));
                $value = str_replace(PHP_EOL, ' ',str_replace($valDelimit, " ", $value));
                $txtColomns .= empty($txtColomns) ? $valDelimit.$key.$valDelimit : $colDelimit.$valDelimit.$key.$valDelimit;
                $txtValues .= empty($txtValues) ? $valDelimit.$value.$valDelimit : $colDelimit.$valDelimit.$value.$valDelimit;
                //Format pour historisation
                if(in_array($key, ['CODE_DEPOT','DECLARATION_ID'/*,'NCC','EXERCICE'*/]) && (int)$value > (int)$this->lastImpotId){
                    //$keyIns = in_array($key, ['CODE_DEPOT','DECLARATION_ID']) ? 'impot_id' : strtolower($key);
                    //$listDec[$item->NCC][$keyIns] = $value;
                    $this->lastImpotId = $value;
                    $this->listImpotId[] = $value;
                }
            }
            if(!$isWriteHeader && !$this->isWriteHeaderInFile){
                fwrite($fp, $txtColomns.PHP_EOL);
                $isWriteHeader = true;
                $this->isWriteHeaderInFile = true;
            }
            //var_dump($txtValues);
            fwrite($fp, $txtValues.PHP_EOL);
            if(!empty($limitSize) && number_format($this->getFileSize($fp)/1048576,2) > ($limitSize-2)){
                $this->filePart++;
                $this->fileSurfix = "_{$this->exportRef}_{$this->colGroupTag}_({$this->nombreNcc})_part{$this->filePart}";
                fclose($fp);
                file_put_contents($this->getFileName(), '');//Erase old data
                $this->isWriteHeaderInFile = false;
                $fp = fopen($this->getFileName(), 'a');
                /////////////fwrite($fp, pack("CCC",0xef,0xbb,0xbf));
                //break;
            }
            $this->loading();
        }
        
        fclose($fp);
        //$this->saveOnSqLite($listDec, $this->args['-f']);
        $this->saveOnSqLite($this->lastImpotId, $this->args['-f'], $this->listImpotId);
    }
    
    public function exportFormatExcel($data, $limitSize=null, $dotExport) {
        $spreadSheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        
        $spreadSheet->getActiveSheet();
        
        $lineIndex = 2;
        $azRange = range('A','Z');
        $azRangeNiv2 = range('A','Z');
        $azRangeNiv3 = range('A','Z');
        //foreach ($data as $item) {
        while ($item = oci_fetch_object($data)) {
            if(isset($item->CODE_DEPOT) && in_array($item->CODE_DEPOT, $dotExport)){ continue; }
            $colIndex = 0;
            
            foreach ($item as $key => $value) {
                if($lineIndex===2){
                    $spreadSheet->getActiveSheet()->setCellValue($azRange[$colIndex].'1', utf8_decode($key));
                }
                
                if($colIndex <= 26){ //Colonne niveau 1 (A1, A2, A3...)
                    if(preg_match("#^-#", $value) || preg_match("#^=#", $value) || preg_match("#^\+#", $value)){
                        $value = '="'.$value.'"';
                    }
                    $spreadSheet->getActiveSheet()->setCellValue($azRange[$colIndex].$lineIndex, ($value));
                    
                }else if($colIndex <= 52){ //Colonne niveau 2 (AA1, AB2, AC3...)
                    
                    
                }else{ //Colonne niveau 2 (AA1, AB2, AC3...)
                    
                }
                
                $colIndex++;
            }
            //if($lineIndex===50) break;
            
            $lineIndex++;
            $this->loading();
        }
        
        
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadSheet);
        //$writer->save($this->filePath.(empty($this->fileSurfix)?'':'_'.$this->fileSurfix).'.xlsx'); 
        $writer->save($this->getFileName());
    }
    
    public function wlog($text, $mode='a') {
        $fp = fopen($this->filePath.'log_export.log', $mode);//opens file in append mode
        fwrite($fp, $text);
        fclose($fp);
        
        echo $text;
    }
    
    public function loading() {
        if($this->loading===10000){
            $this->wlog('=');
//            $fp = fopen($this->filePath.'log_export.log', $mode);//opens file in append mode
//            fwrite($fp, '=');
//            fclose($fp);
            $this->loading = 0;
        }
        $this->loading++;
    }
    
    public function getFileSize($handle){
        fseek($handle, 0, SEEK_END);
        $size   = ftell($handle);
        fseek($handle, 0);

        return $size;
    }
            
    function __destruct() {
        $this->wlog(PHP_EOL.'Fin de l\'export : '.date('d/m/Y H:i:s').PHP_EOL);
        //file_put_contents($this->filePath.'log_export.log', 'Debut de l\'export : '.date().PHP_EOL);
    }
    
//    function getSizeUnits($bytes){
//        if ($bytes >= 1073741824)
//        {
//            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
//        }
//        elseif ($bytes >= 1048576)
//        {
//            $bytes = number_format($bytes / 1048576, 2) . ' MB';
//        }
//        elseif ($bytes >= 1024)
//        {
//            $bytes = number_format($bytes / 1024, 2) . ' KB';
//        }
//        elseif ($bytes > 1)
//        {
//            $bytes = $bytes . ' bytes';
//        }
//        elseif ($bytes == 1)
//        {
//            $bytes = $bytes . ' byte';
//        }
//        else
//        {
//            $bytes = '0 bytes';
//        }
//
//        return $bytes;
//    }
}
