<?php return array (
  'root' => 
  array (
    'pretty_version' => 'No version set (parsed as 1.0.0)',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'kjkoffi/kjk-ora-export-helper',
  ),
  'versions' => 
  array (
    'alchemy/zippy' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '915d604f9e45f757638d06af886cf068acce98c0',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.8',
      'version' => '1.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1958a744696c6bb3bb0d28db2611dc11610e78af',
    ),
    'ifsnop/mysqldump-php' => 
    array (
      'pretty_version' => 'v2.9',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc9c119fe5d70af9a685cad6a8ac612fd7589e25',
    ),
    'kjkoffi/kjk-ora-export-helper' => 
    array (
      'pretty_version' => 'No version set (parsed as 1.0.0)',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.39',
      'version' => '4.4.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '72a5b35fecaa670b13954e6eaf414acbe2a67b35',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.25.0',
      'version' => '1.25.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.41',
      'version' => '4.4.41.0',
      'aliases' => 
      array (
      ),
      'reference' => '9eedd60225506d56e42210a70c21bb80ca8456ce',
    ),
  ),
);
