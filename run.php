<?php
    if (php_sapi_name() != 'cli') {
        throw new Exception('Cette application doit être exécutée en ligne de commande.');
    }

    require 'vendor/autoload.php';
    use Kjk\Bootstrap;
    
    mb_internal_encoding("UTF-8");
    
    $app = new Bootstrap($argv);
    
    $app->run();